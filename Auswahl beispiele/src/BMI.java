import java.util.Scanner;
public class BMI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner einlesen = new Scanner(System.in);
	    int wert, wert2; 
	    String ausgabe = "";
	    String eingabe;
	    
	    double kilogramm;
	    double kilogramm_bmi;
	    double gewicht; 
	    String geschlecht;
	    double bmi;
	    boolean männlich;
	    
	    eingabe="Bitte geben Sie Ihre Körpergröße in [cm] an: ";
	    System.out.println("");
	    System.out.print(eingabe);
	    kilogramm = einlesen.nextDouble(); //Eingabe einer Körpergröße in cm
	    eingabe="Bitte geben Sie jetzt Ihr Gewicht in [kg] an:";
	    System.out.print(eingabe);
	    gewicht = einlesen.nextDouble(); //Eingabe des gewichtes in Kilogramm
	    eingabe="Als letztes geben Sie bitte Ihr Geschlecht an [m/w]:";
	    System.out.print(eingabe);
	    geschlecht = einlesen.next(); //Bestimmung ob geschlecht bei "m", oder weiblich bei "w"
	    
	    
	    kilogramm_bmi = kilogramm/100;
	    bmi = gewicht/(kilogramm_bmi*kilogramm_bmi);
	    
	    if (geschlecht.contains("m")){
	      if (bmi<20){
	        ausgabe="Sie swind Untergewichtig " + bmi;
	      }
	      else if(bmi>=20 && bmi<=25){
	        ausgabe="Sie haben Normalgewicht " + bmi;
	      }
	      else{
	        ausgabe="Sie sind Fett " + bmi;
	      }
	    }
	    else if (geschlecht.contains("w")){
	      if (bmi<19){
	        ausgabe="Sie sind Untergewichtig " + bmi;
	      }
	      else if(bmi>=19 && bmi<=24){
	        ausgabe="Sie haben Normalgewicht " + bmi;
	      }
	      else{
	        ausgabe="Sie sind Fett " + bmi;
	      }
	    }
	    else{
	      ausgabe="Das ist kein Geschlecht";
	    }
	    
	    System.out.println("");
	    System.out.println(ausgabe);    
	    
	}

}
